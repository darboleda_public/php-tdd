<?php

namespace Api\Exceptions;

/**
 *
 */
class ApiExceptionTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Test ApiException
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testCard()
    {   
        $exception = new ApiException('a exception');
        $this->assertTrue(is_numeric(stripos($exception->__toString(), 'ApiException'))); 
    }
}
