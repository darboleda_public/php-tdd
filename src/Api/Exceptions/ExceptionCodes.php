<?php

namespace Api\Exceptions;

class ExceptionCodes
{
    const CARD_INVALID_DATA = 100;
    const TRANSPORT_MISSING_DATA = 200;
    const TRANSPORT_INVALID_TYPE = 201;
    const PLACE_MISSING_DATA = 300;
}
