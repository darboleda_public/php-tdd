<?php

namespace Api\Places;

/**
 *
 */
class PlaceTest extends \PHPUnit_Framework_TestCase
{
    const PLACE_NAME = 'Berlin';
    const PLACE_ID = 1;

    /**
     * Test Place's constructor
     *
     * @return  Place $place
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testPlace()
    {
        $place = new Place(self::PLACE_NAME, self::PLACE_ID);
        $this->assertInstanceOf(Place::class, $place);
        return $place;
    }

    /**
     * Place dataProvider
     * @return array
     */
    public function placeDataprovider()
    {
        return [
            'placeDataprovider' => [new Place(self::PLACE_NAME, self::PLACE_ID)]
        ];
    }

    /**
     * @dataProvider placeDataprovider
     *
     * @return Place $place
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testSetName(Place $place)
    {
        $this->assertInstanceOf(Place::class, $place->setName(self::PLACE_NAME));
        return $place;
    }

    /**
     * @dataProvider placeDataprovider
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetName(Place $place)
    {
        $place->setName(self::PLACE_NAME);
        $this->assertEquals(self::PLACE_NAME, $place->getName());
    }

    /**
     * @dataProvider placeDataprovider
     * @param  Place  $place
     * @return Place $place
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testSetId(Place $place)
    {
        $this->assertInstanceOf(Place::class, $place->setId(self::PLACE_ID));
        return $place;
    }

    /**
     * @dataProvider placeDataprovider
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetId(Place $place)
    {
        $place->setId(self::PLACE_ID);
        $this->assertEquals(self::PLACE_ID, $place->getId());
    }
}
