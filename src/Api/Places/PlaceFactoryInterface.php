<?php

namespace Api\Places;

interface PlaceFactoryInterface
{
    /**
     * Get a place
     * @param  array  $placeData with keys ['name', 'id']
     * @return PlaceInteface $placeInteface
     */
    public function getPlace(array $placeData): AbstractPlace;
}
