<?php

namespace Api\Exceptions\Cards;

use Api\Exceptions\ExceptionCodes;
use Api\Exceptions\ApiException;

class CardFactoryInvalidCardException extends ApiException
{
    /**
     * @param string       $message
     * @param long         $code
     * @param Exception|null $previous
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function __construct(
        $message,
        $code = ExceptionCodes::CARD_INVALID_DATA,
        Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
