<?php

require "vendor/autoload.php";

use Api\TripSorter;
use Api\Cards\CardFactory;
use Api\Transportation\TransportFactory;
use Api\Places\PlaceFactory;

$tripSorter = new TripSorter(
    new CardFactory(new TransportFactory(), new PlaceFactory())
);

$unsortCards = [];

$unsortCards[] =  [
    'transport' => [
        'type' => 'flight',
        'seat' => '7B',
        'name' => 'SK22'
    ],
    'origin' => [
        'name' => 'Stockholm',
        'id' => '20'
    ],
    'destination' => [
        'name' => 'Duabi',
        'id' => '200'
    ],
    'gate' => '18A',
    'baggageMessage' => 'Baggage will we automatically transferred from your last leg.'
];


$unsortCards[] =  [
    'transport' => [
        'type' => 'train',
        'seat' => '12A',
        'name' => '78A'
    ],
    'origin' => [
        'name' => 'Barcelona',
        'id' => '1'
    ],
    'destination' => [
        'name' => 'Gerona Airport',
        'id' => '2'
    ],
    'gate' => '24',
    'baggageMessage' => ''
];

$unsortCards[] =  [
    'transport' => [
        'type' => 'flight',
        'seat' => '7B',
        'name' => 'SK22'
    ],
    'origin' => [
        'name' => 'Gerona Airport',
        'id' => '2'
    ],
    'destination' => [
        'name' => 'Stockholm',
        'id' => '20'
    ],
    'gate' => '22',
    'baggageMessage' => 'Baggage will we automatically transferred from your last leg.'
];

$unsortCards[] =  [
    'transport' => [
        'type' => 'bus',
        'seat' => '',
        'name' => 'airport'
    ],
    'origin' => [
        'name' => 'Madrid',
        'id' => '15'
    ],
    'destination' => [
        'name' => 'Barcelona',
        'id' => '1'
    ],
    'gate' => '',
    'baggageMessage' => ''
];

$orderedList = $tripSorter->sort($unsortCards);

foreach ($orderedList as $card) {
    echo "\n".$card->getItinerary();
}
