To execute tests:

1. Goto root Folder (Trip)
2. Execute command: composer install
3. Execute command: phpunit tests/

The algorithm is pretty simple and consists only of two steps: 

1. In the first step: indexing all origin and destination in a hash.

2. In the second step: Chaining the origin and destination using the indexed hash.

Here it is the code of the sorting algorithm:

Firs step indexing:
    
    /**
     * @param  array $listUnsortCards
     * @return array with keys ['origin','destination']
     */
    public function prepareData($listUnsortCards)
    {
        $origin = [];
        $destination = [];
        foreach ($listUnsortCards as $card) {
            $origin[$card->getOrigin()->getId()] = $card;
            $destination[$card->getDestination()->getId()] = $card;
        }

        return ['origin' => $origin, 'destination' => $destination];
    }

Second step chaining:

    /**
     * @param  array $origin      indexed origins
     * @param  array $destination indexed destinations
     * @param  array $merged merged trip
     * @return array
     */
    public function recursiveSort($origin, $destination, $merged)
    {
        $isMerged = false;
        if (isset($destination[$merged[0]->getOrigin()->getId()])) {
            array_unshift(
                $merged,
                $destination[$merged[0]->getOrigin()->getId()]
            );
            $isMerged = true;
        }

        if (isset($origin[$merged[count($merged)-1]->getDestination()->getId()])) {
            array_push(
                $merged,
                $origin[$merged[count($merged)-1]->getDestination()->getId()]
            );
            $isMerged = true;
        }

        if ($isMerged) {
            return $this->recursiveSort($origin, $destination, $merged);
        } else {
            $merged[count($merged)-1]->appendToBaggageMessage(); //Add final welcome message
            return $merged;
        }
    }
    
To use simply pass the unsorted list to the sort method (I could use the name of the city to generate the hash and the result will be the same, but in real life every data source has an ID, because the name of the city will change in different languages). 
In the file src/index.php you can see an example:

    use Api\TripSorter;
    use Api\Cards\CardFactory;
    use Api\Transportation\TransportFactory;
    use Api\Places\PlaceFactory;
    
    $tripSorter = new TripSorter(
        new CardFactory(new TransportFactory(), new PlaceFactory())
    );
    
    $unsortCards = [];
    
    $unsortCards[] =  [
        'transport' => [
            'type' => 'flight',
            'seat' => '7B',
            'name' => 'SK22'
        ],
        'origin' => [
            'name' => 'Stockholm',
            'id' => '20'
        ],
        'destination' => [
            'name' => 'Duabi',
            'id' => '200'
        ],
        'gate' => '18A',
        'baggageMessage' => 'Baggage will we automatically transferred from your last leg.'
    ];
    
    
    $unsortCards[] =  [
        'transport' => [
            'type' => 'train',
            'seat' => '12A',
            'name' => '78A'
        ],
        'origin' => [
            'name' => 'Barcelona',
            'id' => '1'
        ],
        'destination' => [
            'name' => 'Gerona Airport',
            'id' => '2'
        ],
        'gate' => '24',
        'baggageMessage' => ''
    ];
    
    $unsortCards[] =  [
        'transport' => [
            'type' => 'flight',
            'seat' => '7B',
            'name' => 'SK22'
        ],
        'origin' => [
            'name' => 'Gerona Airport',
            'id' => '2'
        ],
        'destination' => [
            'name' => 'Stockholm',
            'id' => '20'
        ],
        'gate' => '22',
        'baggageMessage' => 'Baggage will we automatically transferred from your last leg.'
    ];
    
    $unsortCards[] =  [
        'transport' => [
            'type' => 'bus',
            'seat' => '',
            'name' => 'airport'
        ],
        'origin' => [
            'name' => 'Madrid',
            'id' => '15'
        ],
        'destination' => [
            'name' => 'Barcelona',
            'id' => '1'
        ],
        'gate' => '',
        'baggageMessage' => ''
    ];
    
    $orderedList = $tripSorter->sort($unsortCards);
    
    foreach ($orderedList as $card) {
        echo "\n".$card->getItinerary();
    }
