<?php

namespace Api;

use Api\Cards\CardFactory;
use Api\Cards\CardFactoryInterface;

/**
 *
 */
class TripSorter
{
    /**
     * @var CardFactoryInterface
     */
    private $cardFactory;

    /**
     * TripSorter's constructor
     *
     * @param CardFactoryInterface $cardFactory [description]
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function __construct(CardFactoryInterface $cardFactory)
    {
        $this->cardFactory = $cardFactory;
    }

    /**
     *
     * @param  array  $unsortCards a matrix of array with format
     *  [
     *       'transport' => [
     *           'type' => 'bus',
     *           'seat' => '',
     *           'name' => 'airport'
     *       ],
     *       'origin' => [
     *           'name' => 'Madrid',
     *           'id' => '15'
     *       ],
     *       'destination' => [
     *           'name' => 'Barcelona',
     *           'id' => '1'
     *       ],
     *       'gate' => '',
     *       'baggageMessage' => ''
     *   ]
     * @return array $itinerary array of string with the itinerary
     */
    public function sort(array $unsortCards)
    {
        //Build and validate data
        $cards = [];
        foreach ($unsortCards as $tmpCard) {
            $cards[] = $this->cardFactory->buildCard($tmpCard);
        }

        //prepare data: index origin and destination
        $indexedAirports = $this->prepareData($cards);

        //process data: sort cards
        $merged = [];
        $merged[] = $cards[0];
        $itinerary = $this->recursiveSort(
            $indexedAirports['origin'],
            $indexedAirports['destination'],
            $merged
        );
        return $itinerary;
    }

    /**
     * @param  array $listUnsortCards
     * @return array with keys ['origin','destination']
     */
    public function prepareData($listUnsortCards)
    {
        $origin = [];
        $destination = [];
        foreach ($listUnsortCards as $card) {
            $origin[$card->getOrigin()->getId()] = $card;
            $destination[$card->getDestination()->getId()] = $card;
        }

        return ['origin' => $origin, 'destination' => $destination];
    }

    /**
     * @param  array $origin      indexed origins
     * @param  array $destination indexed destinations
     * @param  array $merged merged trip
     * @return array
     */
    public function recursiveSort($origin, $destination, $merged)
    {
        $isMerged = false;
        if (isset($destination[$merged[0]->getOrigin()->getId()])) {
            array_unshift(
                $merged,
                $destination[$merged[0]->getOrigin()->getId()]
            );
            $isMerged = true;
        }

        if (isset($origin[$merged[count($merged)-1]->getDestination()->getId()])) {
            array_push(
                $merged,
                $origin[$merged[count($merged)-1]->getDestination()->getId()]
            );
            $isMerged = true;
        }

        if ($isMerged) {
            return $this->recursiveSort($origin, $destination, $merged);
        } else {
            $merged[count($merged)-1]->appendToBaggageMessage();
            return $merged;
        }
    }
}
