<?php

namespace Api\Transportation;

interface TransportationInterface
{
    /**
     * Set the Transportation's name
     *
     * @param string $name name of the trasport (ej. BH7)
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setName(string $name);

    /**
     * Get Transportation's name
     * @return string $name
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getName();

    /**
     * Set the Transportation's seat
     *
     * @param string $seat
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setSeat(string $seat);

    /**
     * Get Transportation's seat
     *
     * @return string $seat
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getSeat();
}
