<?php

namespace Api\Transportation;

interface TransportFactoryInterface
{
    /**
     * Get a specific type of transport
     *
     * @param  array $transportInput expected keys ['type', 'name', 'seat']
     * @return AbstractTransport $transport of type Train, Bus or Flight
     */
    public function getTransport(array $transportInput): AbstractTransport;
}