<?php

namespace Api\Exceptions\Transportation;

use Api\Exceptions\ExceptionCodes;
use Api\Exceptions\ApiException;

class TransportFactoryInvalidTransportException extends ApiException
{
    /**
     * @param string       $message
     * @param long         $code
     * @param Exception|null $previous
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function __construct(
        $message,
        $code = ExceptionCodes::TRANSPORT_INVALID_TYPE,
        Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
