<?php

namespace Api\Places;

interface PlaceInterface
{
    /**
     * Set Place's name
     *
     * @param string $name
     * @return  Place $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setName($name);

    /**
     * Get Place's name
     *
     * @return string $name
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getName();

    /**
     * Set Place's ID
     *
     * @param integer $id
     * @return  Place $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setId($id);

    /**
     * Get Place's ID
     *
     * @return integer $id
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getId();
}
