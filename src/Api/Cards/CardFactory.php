<?php

namespace Api\Cards;

use Api\Places\Place;
use Api\Places\AbstractPlace;
use Api\Transportation\AbstractTransport;
use Api\Transportation\Train;
use Api\Transportation\Bus;
use Api\Transportation\Flight;
use Api\Exceptions\Cards\CardFactoryInvalidCardException;
use Api\Transportation\TransportFactoryInterface;
use Api\Places\PlaceFactoryInterface;

/**
 *
 */
class CardFactory implements CardFactoryInterface
{
    /**
     * @var TransportFactoryInterface
     */
    private $transportFactory;

    /**
     * @var PlaceFactoryInterface
     */
    private $placeFactory;

    /**
     * Card Factory constructor
     *
     * @param TransportFactoryInterface $transportFactory
     * @param PlaceFactoryInterface $placeFactory
     */
    public function __construct(
        TransportFactoryInterface $transportFactory,
        PlaceFactoryInterface $placeFactory
    ) {
        $this->transportFactory = $transportFactory;
        $this->placeFactory = $placeFactory;
    }

    /**
     * @param  string            $gate           [description]
     * @param  string            $baggageMessage [description]
     * @param  Place             $origin         [description]
     * @param  Place             $destination    [description]
     * @param  AbstractTransport $transport      [description]
     *
     * @return AbstractCard $card
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getCard(
        string $gate,
        string $baggageMessage,
        AbstractPlace $origin,
        AbstractPlace $destination,
        AbstractTransport $transport
    ): AbstractCard {
        $card = new Card();
        $card->setGate($gate);
        $card->setBaggageMessage($baggageMessage);
        $card->setOrigin($origin);
        $card->setDestination($destination);
        $card->setTransport($transport);
        return $card;
    }

    /**
     *
     * @param  array $card
     * @return AbstractCard $cardInstance
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function buildCard(array $card): AbstractCard
    {
        if (is_array($card) &&
            isset($card['transport']) &&
            isset($card['transport']['type']) &&
            isset($card['transport']['seat']) &&
            isset($card['transport']['name']) &&
            isset($card['origin']) &&
            isset($card['origin']['name']) &&
            isset($card['origin']['id']) &&
            isset($card['destination']) &&
            isset($card['destination']['name']) &&
            isset($card['destination']['id']) &&
            isset($card['gate']) &&
            isset($card['baggageMessage'])
        ) {
            return $this->getCard(
                $card['gate'],
                $card['baggageMessage'],
                $this->placeFactory->getPlace($card['origin']),
                $this->placeFactory->getPlace($card['destination']),
                $this->transportFactory->getTransport($card['transport'])
            );
        } else {
            throw new CardFactoryInvalidCardException('invalid card data');
        }
    }
}
