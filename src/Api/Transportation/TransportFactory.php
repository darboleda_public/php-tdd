<?php

namespace Api\Transportation;

use Api\Exceptions\Transportation\TransportFactoryInvalidTransportException;
use Api\Exceptions\Transportation\TransportFactoryMissingDataException;

class TransportFactory implements TransportFactoryInterface
{
    /**
     * Get a specific type of transport
     *
     * @param  array $transportInput expected keys ['type', 'name', 'seat']
     * @return AbstractTransport $transport of type Train, Bus or Flight
     */
    public function getTransport(array $transportInput): AbstractTransport
    {

        if (!isset($transportInput['type'])) {
            throw new TransportFactoryMissingDataException(
                'missing type of transport'
            );
        }

        if (!isset($transportInput['seat']) ||
            !isset($transportInput['name']) ||
            trim($transportInput['name']) == ''
        ) {
            throw new TransportFactoryMissingDataException(
                'missing data for transport'
            );
        }

        switch ($transportInput['type']) {
            case 'train':
                $transport = new Train();
                break;
            case 'bus':
                $transport = new Bus();
                break;
            case 'flight':
                $transport = new Flight();
                break;
            default:
                throw new TransportFactoryInvalidTransportException(
                    'transport type '.$transportInput['type'].' not suported'
                );
        }
        $transport->setSeat($transportInput['seat']);
        $transport->setName($transportInput['name']);

        return $transport;
    }
}
