<?php

namespace Api\Cards;

use Api\Places\Place;
use Api\Transportation\AbstractTransport;
use Api\Transportation\Train;
use Api\Cards\CardFactory;
use Api\Cards\Card;
use Api\Transportation\TransportFactory;
use Api\Places\PlaceFactory;
use Api\Places\AbstractPlace;
use Prophecy\Argument;

/**
 *
 */
class CardFactoryTest extends \PHPUnit_Framework_TestCase
{

    const GATE = '45B';
    const BAGGAGE_MESSAGE = 'Baggage will we automatically transferred from your last leg.';

    /**
     * Test constructor
     *
     * @return  CardFactory $factory
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testCardFactory()
    {
        $transportFactory = $this->prophesize(TransportFactory::class);
        $placeFactory = $this->prophesize(PlaceFactory::class);
        $factory = new CardFactory(
            $transportFactory->reveal(),
            $placeFactory->reveal()
        );
        $this->assertInstanceOf(CardFactory::class, $factory);
        return $factory;
    }

    /**
     * get a card factory
     * @return array
     */
    public function cardFactoryDataprovider()
    {
        return [
            'cardFactory' => [$this->getCardFactory()]
        ];
    }

    /**
     * return a card factory
     * @return CardFactory $cardFactory
     */
    private function getCardFactory()
    {
        $transportFactory = $this->prophesize(TransportFactory::class);
        $transport = $this->prophesize(AbstractTransport::class);
        $transportFactory
            -> getTransport(Argument::any())
            ->willReturn($transport->reveal());

        $placeFactory = $this->prophesize(PlaceFactory::class);
        $place = $this->prophesize(AbstractPlace::class);
        $placeFactory
            -> getPlace(Argument::any())
            ->willReturn($place->reveal());
        $factory = new CardFactory(
            $transportFactory->reveal(),
            $placeFactory->reveal()
        );
        return $factory;
    }

    /**
     *
     * @dataProvider cardFactoryDataprovider
     *
     * @return CardFactory $factory
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetCard(CardFactory $factory)
    {
        $origin = $this->prophesize(Place::class);
        $destination = $this->prophesize(Place::class);
        $train = $this->prophesize(Train::class);

        $card = $factory->getCard(
            self::GATE,
            self::BAGGAGE_MESSAGE,
            $origin->reveal(),
            $destination->reveal(),
            $train->reveal()
        );

        $this->assertInstanceOf(Card::class, $card);
        return $factory;
    }

    /**
     * @dataProvider cardFactoryDataprovider
     * @expectedException Api\Exceptions\Cards\CardFactoryInvalidCardException
     * @excpectedExceptionCode Api\Exceptions\ExceptionCodes::CARD_INVALID_DATA
     *
     * @return CardFactory $factory
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testBuildCardInvalidCardDataException(CardFactory $factory)
    {
        $data =  [
            'transport' => [
                'type' => 'train',
                'seat' => '12A',
                'name' => '78A'
            ],
            'destination' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'gate' => '24',
            'baggageMessage' => ''
        ];
        $card = $factory->buildCard($data);
        $this->assertInstanceOf(Card::class, $card);
        return $factory;
    }

    /**
     * @dataProvider cardFactoryDataprovider
     *
     * @return CardFactory $factory
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testBuildCardTrain(CardFactory $factory)
    {
        $data =  [
            'transport' => [
                'type' => 'train',
                'seat' => '12A',
                'name' => '78A'
            ],
            'origin' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'destination' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'gate' => '24',
            'baggageMessage' => ''
        ];
        $card = $factory->buildCard($data);
        $this->assertInstanceOf(Card::class, $card);
        return $factory;
    }

    /**
     * @dataProvider cardFactoryDataprovider
     *
     * @return CardFactory $factory
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testBuildCardBus(CardFactory $factory)
    {
        $data =  [
            'transport' => [
                'type' => 'bus',
                'seat' => '',
                'name' => 'airport'
            ],
            'origin' => [
                'name' => 'Madrid',
                'id' => '15'
            ],
            'destination' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'gate' => '',
            'baggageMessage' => ''
        ];
        $card = $factory->buildCard($data);
        $this->assertInstanceOf(Card::class, $card);
        return $factory;
    }

    /**
     * @dataProvider cardFactoryDataprovider
     *
     * @return CardFactory $factory
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testBuildCardFlight(CardFactory $factory)
    {
        $data =  [
            'transport' => [
                'type' => 'flight',
                'seat' => '7B',
                'name' => 'SK22'
            ],
            'origin' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'destination' => [
                'name' => 'Stockholm.',
                'id' => '20'
            ],
            'gate' => '22',
            'baggageMessage' => 'Baggage will we automatically transferred from your last leg'
        ];
        $card = $factory->buildCard($data);
        $this->assertInstanceOf(Card::class, $card);
        return $factory;
    }
}
