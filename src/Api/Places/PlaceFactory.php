<?php

namespace Api\Places;

use Api\Exceptions\Places\PlaceFactoryMissingDataException;

class PlaceFactory implements PlaceFactoryInterface
{
    /**
     * Get a place
     * @param  array  $placeData with keys ['name', 'id']
     * @return PlaceInteface $placeInteface
     */
    public function getPlace(array $placeData): AbstractPlace
    {
        if (!isset($placeData['name']) || !isset($placeData['id'])) {
            throw new PlaceFactoryMissingDataException('Missing data for place');
        }

        $place = new Place($placeData['name'], $placeData['id']);
        return $place;
    }
}
