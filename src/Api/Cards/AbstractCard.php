<?php

namespace Api\Cards;

use Api\Places\AbstractPlace;
use Api\Transportation\AbstractTransport;
use Api\Cards\CardInterface;

/**
 *
 */
abstract class AbstractCard implements CardInterface
{
    const WELCOME_MESSAGE = 'You have arrived at your final destination.';

    const SEPARATOR = "\n";

    /**
     * Origin
     * @var AbstractPlace
     */
    protected $origin;

    /**
     * Destination
     * @var AbstractPlace
     */
    protected $destination;

    /**
     * Transportation
     * @var AbstractTransport
     */
    protected $transport;

    /**
     * Gate
     * @var string
     */
    protected $gate;

    /**
     * Baggage message
     * @var string
     */
    protected $baggageMessage;

    /**
     * Set Card's origin
     *
     * @param AbstractPlace $origin [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setOrigin(AbstractPlace $origin)
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * Get Card's origin
     *
     * @return  AbstractPlace $origin
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getOrigin(): AbstractPlace
    {
        return $this->origin;
    }

    /**
     * Set Card's destination
     *
     * @param AbstractPlace $destination [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setDestination(AbstractPlace $destination)
    {
        $this->destination = $destination;
        return $this;
    }

    /**
     * Get Card's destination
     *
     * @return  AbstractPlace $origin
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getDestination(): AbstractPlace
    {
        return $this->destination;
    }

    /**
     * Set Card's transport
     *
     * @param AbstractTransport $transport [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setTransport(AbstractTransport $transport)
    {
        $this->transport = $transport;
        return $this;
    }

    /**
     * Get Card's transport
     *
     * @return  AbstractTransport $transport
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getTransport(): AbstractTransport
    {
        return $this->transport;
    }

    /**
     * Set Card's gate
     *
     * @param string $gate [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setGate(string $gate)
    {
        $this->gate = $gate;
        return $this;
    }

    /**
     * Get Card's gate
     *
     * @return  string $gate
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getGate(): string
    {
        return $this->gate;
    }

    /**
     * Set Card's baggageMessage
     *
     * @param string $baggageMessage [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setBaggageMessage(string $baggageMessage)
    {
        $this->baggageMessage = $baggageMessage;
        return $this;
    }

    /**
     * Get Card's baggageMessage
     *
     * @return  string $baggageMessage
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getBaggageMessage(): string
    {
        return $this->baggageMessage;
    }

    /**
     * Append text to Card's baggageMessage
     *
     * @param string $baggageMessage [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function appendToBaggageMessage(
        string $baggageMessage = self::WELCOME_MESSAGE
    ) {
        $this->baggageMessage .= self::SEPARATOR.$baggageMessage;
        return $this;
    }
}
