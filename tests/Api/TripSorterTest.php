<?php

namespace Api;

use Api\TripSorter;
use Api\Cards\CardFactory;
use Api\Transportation\TransportFactory;
use Api\Cards\CardFactoryInterface;
use Api\Places\PlaceFactory;
use Api\Cards\Card;
use Api\Places\AbstractPlace;
use Prophecy\Argument;

/**
 *
 */
class TripSorterTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Test constructor
     *
     * @return  TripSorter $factory
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testTripSorter()
    {
        $cardFactory = $this->prophesize(CardFactory::class);
        $tripSorter = new TripSorter($cardFactory->reveal());
        $this->assertInstanceOf(TripSorter::class, $tripSorter);
        return $tripSorter;
    }

    /**     *
     * @return array
     */
    public function tripSorterDataprovider()
    {
        /*$cardFactory = $this->prophesize(CardFactory::class);
        $place = $this->prophesize(AbstractPlace::class);
        $card = $this->prophesize(Card::class);
        $card->getOrigin()
            ->willReturn($place->reveal());
        $card->getDestination()
            ->willReturn($place->reveal());
        $card->getItinerary()
            ->willReturn('');

        $cardFactory
            ->buildCard(Argument::any())
            ->willReturn($card->reveal());
        $tripSorter = new TripSorter($cardFactory->reveal());*/

        $tripSorter = new TripSorter(
            new CardFactory(new TransportFactory(), new PlaceFactory())
        );

        return [
            'tripSorter' => [$tripSorter]
        ];
    }

    /**
     * [sortTest description]
     * @dataProvider tripSorterDataprovider
     * @return [type] [description]
     */
    public function testSort(TripSorter $tripSorter)
    {
        $unsortCards[] =  [
            'transport' => [
                'type' => 'train',
                'seat' => '12A',
                'name' => '78A'
            ],
            'origin' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'destination' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'gate' => '24',
            'baggageMessage' => ''
        ];

        $unsortCards[] =  [
            'transport' => [
                'type' => 'bus',
                'seat' => '',
                'name' => 'airport'
            ],
            'origin' => [
                'name' => 'Madrid',
                'id' => '15'
            ],
            'destination' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'gate' => '',
            'baggageMessage' => ''
        ];

        $unsortCards[] =  [
            'transport' => [
                'type' => 'flight',
                'seat' => '7B',
                'name' => 'SK22'
            ],
            'origin' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'destination' => [
                'name' => 'Stockholm',
                'id' => '20'
            ],
            'gate' => '22',
            'baggageMessage' => 'Baggage will we automatically transferred from your last leg'
        ];

        $listCards = $tripSorter->sort($unsortCards);
    }

    /**
     * @dataProvider tripSorterDataprovider
     * @return [type] [description]
     */
    public function testSortSingle(TripSorter $tripSorter)
    {
        $unsortCards[] =  [
            'transport' => [
                'type' => 'train',
                'seat' => '12A',
                'name' => '78A'
            ],
            'origin' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'destination' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'gate' => '24',
            'baggageMessage' => ''
        ];

        $listCards = $tripSorter->sort($unsortCards);
    }

    /**
     * @dataProvider tripSorterDataprovider
     * @return [type] [description]
     */
    public function testSortNext(TripSorter $tripSorter)
    {
        $unsortCards[] =  [
            'transport' => [
                'type' => 'train',
                'seat' => '12A',
                'name' => '78A'
            ],
            'origin' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'destination' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'gate' => '24',
            'baggageMessage' => ''
        ];

        $unsortCards[] =  [
            'transport' => [
                'type' => 'flight',
                'seat' => '7B',
                'name' => 'SK22'
            ],
            'origin' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'destination' => [
                'name' => 'Stockholm',
                'id' => '20'
            ],
            'gate' => '22',
            'baggageMessage' => 'Baggage will we automatically transferred from your last leg'
        ];

        $unsortCards[] =  [
            'transport' => [
                'type' => 'bus',
                'seat' => '',
                'name' => 'airport'
            ],
            'origin' => [
                'name' => 'Madrid',
                'id' => '15'
            ],
            'destination' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'gate' => '',
            'baggageMessage' => ''
        ];
        $itinerary = $tripSorter->sort($unsortCards);
    }
}
