<?php

namespace Api\Places;

/**
 *
 */
class PlaceFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test PlaceFactory's constructor
     *
     * @return  PlaceFactory $train
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testPlaceFactory()
    {
        $placeFactory = new PlaceFactory();
        $this->assertInstanceOf(PlaceFactory::class, $placeFactory);
        return $placeFactory;
    }

    /**
     * Return a standard transport factory
     * @return PlaceFactory standar transport factory
     */
    public function placeFactoryProvider()
    {
        return [
            'placeFactory' => [new PlaceFactory()]
        ];
    }

    /**
     * @dataProvider placeFactoryProvider
     * @expectedException  Api\Exceptions\Places\PlaceFactoryMissingDataException
     * @expectedExceptionCode Api\Exceptions\ExceptionCodes::PLACE_MISSING_DATA
     *
     * @return [type] [description]
     */
    public function testGetPlaceMissingData(PlaceFactory $placeFactory)
    {
        $placeFactory->getPlace([]);
    }

    /**
     * @dataProvider placeFactoryProvider
     *
     * @return [type] [description]
     */
    public function testGetPlace(PlaceFactory $placeFactory)
    {
        $place = $placeFactory->getPlace(['name'=>'Berlin', 'id' => '1']);
        $this->assertInstanceOf(Place::class, $place);
    }
}
