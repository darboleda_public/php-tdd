<?php

namespace Api\Places;

/**
 *
 */
class Place extends AbstractPlace
{
    /**
     * Set Place's name
     *
     * @param string $name
     * @return  Place $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get Place's name
     *
     * @return string $name
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Place's ID
     *
     * @param integer $id
     * @return  Place $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get Place's ID
     *
     * @return integer $id
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getId()
    {
        return $this->id;
    }
}
